<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\data_siswaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

Route::post('/', [LoginController::class, 'postLogin'])->name('postLogin');

Route::get('/dataSiswa', [data_siswaController::class, 'index'])->name('dataSiswa');
Route::get('/createSiswa', [data_siswaController::class, 'create'])->name('createSiswa');
Route::post('/simpanSiswa', [data_siswaController::class, 'store'])->name('simpanSiswa');
Route::get('/editSiswa/{id_siswa}', [data_siswaController::class, 'edit'])->name('editSiswa');
Route::post('/updateSiswa/{id_siswa}', [data_siswaController::class, 'update'])->name('updateSiswa');
Route::get('/deleteSiswa/{id_siswa}', [data_siswaController::class, 'destroy'])->name('deleteSiswa');