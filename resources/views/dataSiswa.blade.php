<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/dataSiswa.css')}}">
    <link rel="icon" href="{{ asset('assets/img/logo.ico') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous">
    <title>Data Siswa</title>  

  </head>
   <body>
    <br>
    <br>
    <div class="jumbotron jumbotron-fluid">
      <div class="container">
        <div class="row">
        <!-- grid col data siswa -->
            <div class="col-12">
                <h1 style="padding: 0 14px;">Data Siswa <a href="{{ route('createSiswa') }}" class="btn btn-info btn-md float-right text-light" role="button" >Tambah Data Siswa</a></h1>
            </div>
        </div>
            <!-- table ke database -->
            <div class="container">
              <div class="row">
                <div class="col-12">
                  <table class="table table-bordered table-hover">
                  <thead class="thead-dark">
                      <tr class="text-align-center">
                          <th>No.</th>
                          <th>Nis Siswa</th>
                          <th>Nama Siswa</th>
                          <th>Sekolah Siswa</th>
                          <th>Jenis Kelamin</th>
                          <th>Opsi</th>
                      </tr>
                  </thead>
                      @foreach($dtSiswa as $item)
                      <tr class="text-align-center">
                          <td>{{ $loop->iteration }}</td>
                          <td>{{ $item->nis_siswa }}</td>
                          <td>{{ $item->nama_siswa }}</td>
                          <td>{{ $item->sekolah_siswa }}</td>
                          <td>{{ $item->jk_siswa }}</td>
                          <td class="btn-crud">
                            <a href="{{ url('editSiswa', $item->id_siswa) }}" class=" btn btn-warning btn-sm"><i class="fas fa-edit"></i> Edit Data</a>
                            <a href="{{ url('deleteSiswa', $item->id_siswa) }}" class=" btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i> Hapus Data</a>
                          </td>
                      </tr>
                      @endforeach
                  </table>
                </div>
              </div>
            </div>

      </div>
    </div>

            <!-- akhir table ke database -->
    <!-- wrapper/ REQUIRED SCRIPTS --> 
    @include('sweetalert::alert')   
    
    <!-- Script Ajax untuk confirm delete -->
    <script>
      
    </script>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>

<!--Java Script  -->
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<!-- Akhir JavaScript -->
  </body>
</html>