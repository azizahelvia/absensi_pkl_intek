<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCatatanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catatan', function (Blueprint $table) {
            $table->increments('id_catatan');
            $table->integer('id_user')->length(10);
            $table->integer('id_bln')->length(10);
            $table->integer('id_hari')->length(10);
            $table->integer('id_tgl')->length(10);
            $table->longText('isi_catatan');
            $table->enum('status_catatan', ['Menunggu, Dikonfirmasi, Ditolak']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catatan');
    }
}
