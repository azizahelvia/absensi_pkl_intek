<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAbsensiMagangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('absensi_magang', function (Blueprint $table) {
            $table->increments('id_absen');
            $table->string('id_user', 100);
            $table->integer('id_bln')->length(10)->unsigned();
            $table->integer('id_hari')->length(10)->unsigned();
            $table->integer('id_tgl')->length(10)->unsigned();
            $table->timestamp('jam_masuk')->nullable()->default(null);
            $table->enum('status_jam_msk', ['Menunggu, Dikonfirmasi']);
            $table->timestamp('jam_keluar')->nullable()->default(null);
            $table->enum('status_jam_kel', ['Menunggu, Dikonfirmasi']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('absensi_magang');
    }
}
