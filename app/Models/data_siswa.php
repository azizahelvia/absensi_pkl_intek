<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class data_siswa extends Model
{
    public $timestamps = false;

    protected $table = "data_siswa";
    protected $primaryKey = "id_siswa";
    protected $fillable = ['id_siswa', 'nis_siswa', 'nama_siswa', 'sekolah_siswa', 'jk_siswa'];
}
